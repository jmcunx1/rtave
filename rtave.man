.\"
.\" Copyright (c) 2021 ... 2024 2025
.\"     John McCue
.\"
.\" Permission to use, copy, modify, and distribute this software
.\" for any purpose with or without fee is hereby granted,
.\" provided that the above copyright notice and this permission
.\" notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
.\" WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
.\" WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
.\" FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
.\" SOFTWARE.
.\"
.TH RTAVE 1 "2021-02-03" "JMC" "User Commands"
.SH NAME
rtave - Determine Average Time Difference
.SH SYNOPSIS
rtave [OPTIONS]... [FILE]...
.SH DESCRIPTION
Reads one or more Files and Prints the Average
Seconds Difference for all Valid Records between
Start Date/Time and End Date/Time.
.PP
If no files (FILE) are specified on the command line or
if FILE has name '-', stdin (Standard Input) is used.
.TP
-a
Process all Records.
If not specified, all entries that
generate a difference greater that 24 hours
will be ignored.
.TP
-e file
Optional, if used, write error messages to file 'file'.
If not specified, errors written to stderr.
.TP
-f
Optional, Force file create.
Create file even if the target file exists.
.TP
-h
Show brief help and exit.
.TP
-o file
Optional, if used, write output to file 'file'.
If not specified, output written to stdout.
.TP
-V
Output version information and exit.
.TP
-v
Optional, Verbose Level.
Print information about the run on stderr.
Default is to only print errors on stderr.
.nf
    Level  Meaning
    -----  -----------------------------------------------------
    = 0    Print Errors on stderr
    >= 1   Above plus Additional information about Files Read
    >= 2   Above plus warnings about Data Records
    >= 3   Above plus show Detail Data processed
.fi

.SH INPUT FORMAT
This is the format of the Input File,
it must be a flat fixed length File:

.nf
    Pos  Size  Format    Description
    ---  ----  --------  ---------------------------------------
      1    8   YYYYMMDD  Start Date
      9    6   HHMMSS    Start Time
     15    8   YYYYMMDD  End Date
     23    6   HHMMSS    End Time
     29  n/a   string    Used as col 1 when creating a csv file

Example Records:
----------------
2021020208523920210202093709Run Times for Item 1
2021020308523920210203103103Run Times for Item 1
2021020408523920210204113204Run Times for Item 1
.fi
.SH DIAGNOSTICS
On failure, reason for the abort is printed on stderr
.PP
If a difference between Start and End is greater than
24 hours, that entry will be bypassed when determining the Average.
See, '-a' and '-v' above.
.PP
If you specify '-e', in some cases info will still be
written to stderr.
This can occur during Command Line Argument Processing.
.SH SEE-ALSO
awk(1),
bc(1)
.SH ERROR-CODES
.nf
0 success
>0 Failed
.fi
